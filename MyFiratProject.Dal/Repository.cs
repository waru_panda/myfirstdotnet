﻿using MyFirstProject.Common;
using System;
using System.Collections.Generic;
using System.Text;
using MyFirstProject.BL.Contracts.Contracts.Model.Authorization;
using MyFirstProject.BL.Contracts.Contracts.Model.Employees;
using Npgsql;

namespace MyFiratProject.Dal
{
    public class Repository
    {
        private readonly AppSettings _appSettings;
        public Repository(AppSettings appSettings)
        {
            _appSettings = appSettings;
        }

        public TokenModel Get(Guid token)
        {
            var connection = new  Npgsql.NpgsqlConnection(_appSettings.DbConnection);
            var cmd = new Npgsql.NpgsqlCommand("select user_id from tokens where token=:token limit 1", connection);
            cmd.Parameters.Add(new NpgsqlParameter("token", token));
            connection.Open();
            var result = new TokenModel();
            var reader =  cmd.ExecuteReader();
            reader.Read();
            result.UserId = Int32.Parse(reader["user_id"].ToString());
            connection.Close();
            return result;
        }

        public List<UserModel> Get(int userId)
        {
            var connection = new  Npgsql.NpgsqlConnection(_appSettings.DbConnection);
            var cmd = new Npgsql.NpgsqlCommand(@"select u.name as user_name, d2.name as department_name from users u join deparments d2 on u.department_id = d2.id where exists(select 1 from users u2 where u2.id=:userId and (u2.is_admin=TRUE or u2.department_id=d2.id))", connection);
            cmd.Parameters.Add(new NpgsqlParameter("userId", userId));
            connection.Open();
            var result = new List<UserModel>();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var user = new UserModel();
                user.Name = reader["user_name"].ToString();
                user.Department = new DepartmentModel();
                user.Department.Name = reader["department_name"].ToString();
                result.Add(user);
            }
            connection.Close();
            return result;
        }
    }
}
