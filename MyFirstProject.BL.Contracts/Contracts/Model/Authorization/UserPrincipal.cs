using System.Security.Principal;

namespace MyFirstProject.BL.Contracts.Contracts.Model.Authorization
{
    public class UserPrincipal: GenericPrincipal
    {
        public UserPrincipal(IIdentity identity, TokenModel token) : base(identity, new string[] { })
        {
            Token = token;
        }

        public TokenModel Token { get; set; }

        public override bool IsInRole(string role)
        {
            return true;
        }
    }
    
    
}