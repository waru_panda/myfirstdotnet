namespace MyFirstProject.BL.Contracts.Contracts.Model.Employees
{
    public class UserModel
    {
        public string Name { get; set; }
        public DepartmentModel Department { get; set; }
    }
}