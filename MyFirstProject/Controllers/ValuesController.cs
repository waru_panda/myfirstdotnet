﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyFiratProject.Dal;
using MyFirstProject.BL.Contracts.Contracts.Model.Authorization;
using MyFirstProject.BL.Contracts.Contracts.Model.Employees;
using MyFirstProject.BL.Employees;
using MyFirstProject.Common;

namespace MyFirstProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ValuesController : ControllerBase
    {
        
        [HttpGet]
        public List<UserModel> GetUsers([FromServices]MyFirstProjectGetEmployeesHandler handler)
        {
            var userId = ((UserPrincipal)User).Token.UserId;
            var users = handler.Handle(userId);
            return users;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
