using System;
using Microsoft.AspNetCore.Authentication;

namespace MyFirstProject.BL.Core
{
    public static class MyFirstProjectAuthExtensions
    {
        public static AuthenticationBuilder MyFirstProjectAuth(this AuthenticationBuilder builder,
            Action<MyFirstProjectAuthOptions> configureOptions)
        {
            return builder.AddScheme<MyFirstProjectAuthOptions, MyFirstProjectAuthHandler>("MFP Scheme", "MFP Auth",
                configureOptions);
        }
    }
}