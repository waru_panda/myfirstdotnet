using System;
using System.Linq;
using System.Security.Principal;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using MyFiratProject.Dal;
using MyFirstProject.BL.Contracts.Contracts.Model.Authorization;

namespace MyFirstProject.BL.Core
{
    public class MyFirstProjectAuthHandler: AuthenticationHandler<MyFirstProjectAuthOptions>
    {
        Repository _tokenService;
        public MyFirstProjectAuthHandler(IOptionsMonitor<MyFirstProjectAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock,
            Repository tokenService) : base(options, logger, encoder, clock)
        {
            _tokenService = tokenService;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            StringValues tokens;
            if (!Context.Request.Headers.TryGetValue("Authorization", out tokens))
            {
                return AuthenticateResult.Fail(new Exception());
            }
            var user = _tokenService.Get(Guid.Parse(tokens.First()));
            if(user != null)
            {
                Context.User = new UserPrincipal(new GenericIdentity(user.UserId.ToString()), user);
                    return AuthenticateResult.Success(new AuthenticationTicket(Context.User, null, "MFP Scheme"));
            }            
            return AuthenticateResult.Fail(new Exception());
        }
    }
}