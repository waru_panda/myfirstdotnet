using System.Collections.Generic;
using MyFiratProject.Dal;
using MyFirstProject.BL.Contracts.Contracts.Model.Employees;

namespace MyFirstProject.BL.Employees
{
    public class MyFirstProjectGetEmployeesHandler
    {
        Repository _employeeService;

        public MyFirstProjectGetEmployeesHandler(Repository employeeService)
        {
            _employeeService = employeeService;
        }

        public List<UserModel> Handle(int userId)
        {
            var userList = _employeeService.Get(userId);
            return userList;
        }
    }
}